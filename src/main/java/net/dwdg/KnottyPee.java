package net.dwdg;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class KnottyPee extends JavaPlugin implements Listener {
    private final Map<UUID, Integer> tickleCount = new HashMap<>();

    public void onEnable() {
        Bukkit.getPluginManager().registerEvents(this, this);
    }

    @EventHandler
    public void onPlayerTickles(EntityDamageByEntityEvent event) {
        Entity damagedEntity = event.getEntity();
        if (!(damagedEntity instanceof Player)) {
            return; // Player not being damaged
        }
        Player damagee = (Player) damagedEntity;

        Entity damagerEntity = event.getDamager();
        if (!(damagerEntity instanceof Player)) {
            return; // Player did not do the damage
        }

        Player damager = (Player) damagerEntity;
        ItemStack feather = damager.getInventory().getItemInMainHand();
        if (feather.getType() != Material.FEATHER) {
            return; // not holding a feather
        }

        // get current tickle count
        UUID uuid = damagee.getUniqueId();
        int count = tickleCount.containsKey(uuid) ? tickleCount.get(uuid) : 0;

        count++; //increment tickle count

        if (count > 6) {
            // tickle count higher than 6, lets pee everywhere

            ItemStack pee = new ItemStack(Material.INK_SACK);
            //noinspection deprecation
            pee.setTypeId(11); // YELLOW DYE

            final Item entityPee = damagee.getWorld().dropItem(damagee.getLocation(), pee);
            entityPee.setPickupDelay(Integer.MAX_VALUE);

            // remove pee from world 5 seconds later to prevent build up of entities
            new BukkitRunnable() {
                @Override
                public void run() {
                    entityPee.remove();
                }
            }.runTaskLater(this, 5 * 20);
        }

        // store new tickle count
        tickleCount.put(uuid, count);

        // remove this tickle from counter 10 seconds later
        new BukkitRunnable() {
            public void run() {
                if (tickleCount.containsKey(uuid)) {
                    int count = tickleCount.get(uuid) - 1;

                    if (count <= 0) {
                        tickleCount.remove(uuid);
                    } else {
                        tickleCount.put(uuid, count);
                    }
                }
            }
        }.runTaskLater(this, 10 * 20);

        // cancel default damage (stops plugins like mcmmo unarmed skill doing 1 shot tickle deaths)
        event.setCancelled(true);

        // deal our own damage instead (1 heart damage, aka 2 half hearts)
        damagee.setHealth(damagee.getHealth() - 2.0D);
    }
}
